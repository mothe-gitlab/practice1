VPC_CIDR        = "10.10.0.0/16"
PROJECTNAME     = "student"
ENV             = "dev"
PUB_SUBNET_CIDR = ["10.10.0.0/24" , "10.10.1.0/24"]
PRV_SUBNET_CIDR = ["10.10.2.0/24" , "10.10.3.0/24"]
DEFAULT_VPC_ID  = "vpc-0376cb79189f400ac"