resource "aws_vpc_peering_connection" "foo" {
  peer_owner_id = aws_vpc.main.owner_id
  peer_vpc_id   = aws_vpc.main.id
  vpc_id        = var.DEFAULT_VPC_ID

  accepter {
    allow_remote_vpc_dns_resolution = true
  }
}